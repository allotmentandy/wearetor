<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table( 'categories' )->insert( [
        	'id'		 => 1,
        	'name'       => "Solution",
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now(),

      ] );

        DB::table( 'categories' )->insert( [
        	'id'		 => 2,
        	'name'       => "Advertising",
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now(),

      ] );

        DB::table( 'categories' )->insert( [
        	'id'		 => 3,
        	'name'       => "Creative Services",
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now(),

      ] );

        DB::table( 'categories' )->insert( [
        	'id'		 => 4,
        	'name'       => "Digital Services",
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now(),

      ] );

        DB::table( 'categories' )->insert( [
        	'id'		 => 5,
        	'name'       => "Data Services",
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now(),

      ] );

        DB::table( 'categories' )->insert( [
        	'id'		 => 6,
        	'name'       => "Lead Generation",
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now(),

      ] );

        DB::table( 'categories' )->insert( [
        	'id'		 => 7,
        	'name'       => "Telemarketing",
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now(),

      ] );

        DB::table( 'categories' )->insert( [
        	'id'		 => 8,
        	'name'       => "Social Media",
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now(),

      ] );

        DB::table( 'categories' )->insert( [
        	'id'		 => 9,
        	'name'       => "Events and Exhibitions",
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now(),

      ] );

        DB::table( 'categories' )->insert( [
        	'id'		 => 10,
        	'name'       => "Learning Academy",
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now(),

      ] );



    }
}
