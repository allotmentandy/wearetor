<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table( 'products' )->insert( [
        'title'          => "16GB Branded USB Stick with PVC Sleeve and Printed Box",
        'price'          => 1.99,
        'description'    => "description",
        'url'            => "",
        'image'          => "1.jpg",
        'created_at'     => Carbon::now(),
        'updated_at'     => Carbon::now(),
      ] );    

      DB::table( 'products' )->insert( [
        'title'          => "2pp A4 Data Sheet",
        'price'          => 1.99,
        'description'    => "description",
        'url'            => "",
        'image'          => "2.jpg",
        'created_at'     => Carbon::now(),
        'updated_at'     => Carbon::now(),
      ] );    

      DB::table( 'products' )->insert( [
        'title'          => "4pp A4 Data Sheet",
        'price'          => 1.99,
        'description'    => "description",
        'url'            => "",
        'image'          => "3.jpg",
        'created_at'     => Carbon::now(),
        'updated_at'     => Carbon::now(),
      ] );    

      DB::table( 'products' )->insert( [
        'title'          => "4ppp A5 Greetings Cards",
        'price'          => 1.99,
        'description'    => "description",
        'url'            => "",
        'image'          => "4.jpg",
        'created_at'     => Carbon::now(),
        'updated_at'     => Carbon::now(),
      ] );    

      DB::table( 'products' )->insert( [
        'title'          => "Branded Virtual Reality Goggles",
        'price'          => 1.99,
        'description'    => "description",
        'url'            => "",
        'image'          => "5.jpg",
        'created_at'     => Carbon::now(),
        'updated_at'     => Carbon::now(),
      ] );    

      DB::table( 'products' )->insert( [
        'title'          => "Branded Virtual Reality Goggles",
        'price'          => 1.99,
        'description'    => "description",
        'url'            => "",
        'image'          => "6.jpg",
        'created_at'     => Carbon::now(),
        'updated_at'     => Carbon::now(),
      ] );    

      DB::table( 'products' )->insert( [
        'title'          => "E-Book Creation and E-book Publisher",
        'price'          => 1.99,
        'description'    => "description",
        'url'            => "",
        'image'          => "7.jpg",
        'created_at'     => Carbon::now(),
        'updated_at'     => Carbon::now(),
      ] );    

      DB::table( 'products' )->insert( [
        'title'          => "Branded Virtual Reality Goggles",
        'price'          => 1.99,
        'description'    => "description",
        'url'            => "",
        'image'          => "8.jpg",
        'created_at'     => Carbon::now(),
        'updated_at'     => Carbon::now(),
      ] );    

      DB::table( 'products' )->insert( [
        'title'          => "Branded Virtual Reality Goggles",
        'price'          => 1.99,
        'description'    => "description",
        'url'            => "",
        'image'          => "9.jpg",
        'created_at'     => Carbon::now(),
        'updated_at'     => Carbon::now(),
      ] );    

    }
}



