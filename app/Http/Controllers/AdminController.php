<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Products;

class AdminController extends Controller
{
    public function index()
    {

    	$categories = Categories::all();
    	$products = Products::all();

	    return view('admin', [ 'categories' => $categories, 'products' => $products]);
    }

    public function new()
    {

    	$categories = Categories::all();
    	$products = Products::all();

	    return view('adminNew', [ 'categories' => $categories, 'products' => $products]);
    }




}
