<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Products;

class FrontpageController extends Controller
{

    public function index()
    {

    	$categories = Categories::all();
    	$products = Products::all();

	    return view('frontpage', [ 'categories' => $categories, 'products' => $products]);
    }

}
