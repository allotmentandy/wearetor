wearetor test
-------------

###uploaded laravel 
###migration - to run type

```php artisan migrate:fresh```

please note i have set the key length to 191 as i am using mysql 5.7

###Db Seed to populate the database with the first 9 products and 10 categories with images in /public/images

```php artisan db:seed```

###bootstrap frontpage 

bootstrap is already included with laravel!

![Screenshot of frontpage](public/img/wearetor.jpg)

###laravel backend

access it via 
``` /admin```

![Screenshot of ADMIN frontpage](public/img/wearetorAdmin.jpg)

```/admin/new```

![Screenshot of ADMIN add a product](public/img/wearetorAdminNew.jpg)




still to do (in bold): 


4. #Create a basic admin to manage the products, this will need to include: 
	- List view of all products
	- Create new form **processor to do**
	**- Edit product form to do**
	- Although not displayed on the frontend please can the products also store a price and URL
	- Products need to be able to be assigned to multiple categories
5. Style up page to match PDF/InDesign file provided 
**6. Add ability to track clicks on the categories buttons and store this information in the database**
7. Share the repository with scottgib (sgibling@gmail.com)??

Notes/Tips:
**In the folder are some web fonts for the font in the visual**
Please use bootstrap where possible
Use as much Laravel functionality as you can
For the purposes of this demo the admin does not need to be secured behind a login
