<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>We Are Tor test</title>

        <link href="/css/app.css" rel="stylesheet">

        <style>
           .yellow {color: yellow; }
           .white  {color: white;}
           .logo {text-indent:25px; font-weight: bold;}
           button {background: blue; color: white; border:0; font-size: small; padding: 5px 20px 5px 20px;}
           p {line-height: normal;}
           .hero {background-image: url(/img/hero.png); background-size: 100%; }

           .category {background: lightgrey; color: blue; border:1px solid black; font-size: small; padding: 5px 50px 5px 10px; margin: 5px; min-width: 200px; text-align: left;}
           .categoryActive {background: orange; color: white; border:1px solid black; font-size: small; padding: 5px 50px 5px 10px;  margin: 5px;  min-width: 200px; text-align: left;}
           .smallText{font-size:x-small; font-weight: bold;}

        </style>

    </head>
    <body>

    <nav class="navbar navbar-expand-lg bg-dark">
        <a href="#" class="logo"> <span class="yellow">ONE <span class="white" href="#">VISION</span></a>
    </nav>

    <div class="jumbotron hero" >
        <div class="container">
            <center>
                <h2>New! Hyper-personalised promotion</h2>
                <h2>with InMail-to-PointDrive</h2>
                <p>SAP Partners - connect with your key accounts and deliver</br>
                highly compelling content to convert them into buyers<br>
                Create PointDrive sales sites and use InMail to target 10-20 of your <br>
                key sales prospects.
                </p>
                <button>Sign Up ></button>
            </center>
        </div>
    </div>

<div class="container">
  <div class="row">
    <div class="col-3">

@foreach($categories as $category)
    @if ($category->name  === 'Creative Services')
        <button class="categoryActive">{{ $category->name }}</button>
    @else
    <button class="category">{{ $category->name }}</button><br>
    @endif
@endforeach


    </div>

    <div class="col-2">
      <h2>Products</h2>
    </div>

    <div class="col-7">



    <div class="row">
        @foreach($products as $product)
            <div class="col-md-4">
                <img src="/images/{{$product->image}}" width=150 height=150><br>
                <div class="smallText">{{$product->title }}</div>
            </div>
        @endforeach
    </div>

    </div>
  </div>
</div>

    </body>
</html>
