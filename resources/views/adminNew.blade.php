<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>We Are Tor test</title>

        <link href="/css/app.css" rel="stylesheet">

        <style>
           .yellow {color: yellow; }
           .white  {color: white;}
           .logo {text-indent:25px; font-weight: bold;}
           button { color: white; border:0; padding: 5px 20px 5px 20px; float:right;}
           p {line-height: normal;}
           .navbar {margin-bottom: : 20px;}
        </style>

    </head>
    <body>

    <nav class="navbar navbar-expand-lg bg-dark">
        <a href="#" class="logo"> <span class="yellow">ONE <span class="white" href="#">VISION</span> ADMIN SYSTEM</a>
    </nav>
    <hr>

    <div class="container">
      <div class="row">
        <div class="col-3">

        </div>

        <div class="col-9">
            <h3>Products <a href="/admin/new">New Product</a></h3>
            
            <hr>

            <form action="/admin/new/process"  method="POST"  >

            <input name="_token" type="hidden" value="{{ csrf_token() }}">

            Name: <input type="text" name="title">
            <br>
            Price: <input type="text" name="price">
            <br>
            Description: <br><textarea name="description"></textarea>
            <br>
            Url: <input type="text" name="url">
            <br>
            Image: <input type="file" name="fileToUpload" id="fileToUpload">

            <fieldset>
                <legend>Please select categories</legend>
              @foreach ($categories as $value) 
                <input type="checkbox" name="categories" value="{{$value->id}}" /> {{$value->name }}<br>
              @endforeach
            </fieldset>

            <input type="submit" value="submit">

            </form>

        </div>
      </div>
    </div>

    </body>
</html>
